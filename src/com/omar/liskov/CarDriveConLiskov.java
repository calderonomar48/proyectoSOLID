package com.omar.liskov;


/**
 * por que usar LISKOV
 * 1. Se debe programar contra interface
 * 2. Es recomenable tener nuestra solo en los metodos que se exponen por la interface,
 *    y no tenerla en las clases concretas.
 * 3. Liskov se aplicará siempre y cuando se pueda instanciar un objeto de Tipo Clase Concreta y de Tipo Interface
 *
 */
public class CarDriveConLiskov {

    public static void main(String[] args) {

        ICar car;
        car = new Car();

        String cardType = "carzzx";
        if ("car" == cardType) {
            car = new Car();
            car.accelerate();
        } else if ("electric" == cardType) {
            car = new ElectricCar();
            car.accelerate();
        } else {
            throw new RuntimeException("Invalid car");
        }
    }
}
