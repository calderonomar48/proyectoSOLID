package com.omar.liskov;


// https://experto.dev/principios-solid-con-ejemplos/
public class CarDriveSinLiskov {

    public static void main(String[] args) {

        String cardType = "electric";
        if ("car" == cardType) {
            Car car = new Car();
            car.accelerate();
        } else if ("electric" == cardType) {
            ElectricCar electricCar = new ElectricCar();
            if ((electricCar.hasBattery())) {
                electricCar.accelerate();
            }
        } else {
            throw new RuntimeException("Invalid car");
        }
    }
}
