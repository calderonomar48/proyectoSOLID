package com.omar.liskov;


/**
 * El principio de segregacion de interface nos dice que: Cuando
 * se trabaje con una interface y la implementadora necesite otro metodo
 * para su interface, se recomienda crear otra interface para esa dicha clase
 * concreta.
 * Este principio nos dice que la clase concreta no debería tener metodos
 * implementados vacios. Todos los métodos que se declaren en una interface deben
 * tener un cuerpo e su implementación.
 *
 * La solucion es que la clase concreta implemente 2 o mas interface, segun lo necesite.
 * La primera interface es la que se necesita si o si, la segunda interface
 * es la que tiene el metodo unico para esa clase concreta.
 */

public class CuatroInterfaceSegregation extends  B {

}