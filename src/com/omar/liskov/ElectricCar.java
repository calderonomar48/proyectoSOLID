package com.omar.liskov;

public class ElectricCar implements ICar {

    private int battery;

    @Override
    public void accelerate() {
        System.out.println("aceleting the car electric");
    }

    @Override
    public void stop() {
        System.out.println("stoping the car electric");
    }

    public boolean hasBattery() {
        System.out.println("checking battery");
        if (battery < 95) {
            System.out.println("the battery is very low :(");
            return false;
        } else {
            System.out.println("battery OK :)");
            return true;
        }
        System.out.println("ola de nuevo mantiene commits");
    }
}
