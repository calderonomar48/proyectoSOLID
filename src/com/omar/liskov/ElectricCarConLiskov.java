package com.omar.liskov;

public class ElectricCarConLiskov implements ICar {

    private int battery;

    @Override
    public void accelerate() {
        if (hasBattery()) {
            System.out.println("aceleting the car electric");
        } else {
            System.out.println("no acelerA");
        }
    }

    @Override
    public void stop() {
        System.out.println("stoping the car electric");
    }

    private boolean hasBattery() {
        System.out.println("checking battery");
        if (battery < 95) {
            System.out.println("the battery is very low :(");
            return false;
        } else {
            System.out.println("battery OK :)");
            return true;
        }
    }
}
