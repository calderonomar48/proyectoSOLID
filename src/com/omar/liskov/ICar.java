package com.omar.liskov;

interface ICar {

    void accelerate();

    void stop();
}
