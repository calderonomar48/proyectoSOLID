package com.omar.liskov;


/**
 * El principio dependency inversion (Principio de inversion de dependencias)
 * Este patrón nos dice que nuestra código no dependa de clases concreates, sino
 * que debemos abstraerlas a través de interfaces y solo depender de estas abstracciones.
 * Es decir debemos programar contra interfaces. Debemos depender de interfaces sin conocer
 * cuál es su lógica
 *
 *
 * SPRING - INYECCIÓN DE DEPENDENCIAS
 *
 */

public class QuintoDependencyInversion extends  B {

}